import 'package:flutter/material.dart';
import 'package:kostenlos/utils/colors.dart';
import 'package:kostenlos/utils/responsive.dart';

// custom bottom bar
class CusBottomBar extends StatefulWidget {
  const CusBottomBar({Key? key}) : super(key: key);

  @override
  State<CusBottomBar> createState() => _CusBottomBarState();
}

class _CusBottomBarState extends State<CusBottomBar> {
  @override
  Widget build(BuildContext context) {
    // getting query width and height
    double dWidth = MediaQuery.of(context).size.width;
    double dHeight = MediaQuery.of(context).size.height;

    // return bottom bar if device is on mobile view
    return Responsive.isMobile(context)
        ? Container(
            width: dWidth,
            height: (dHeight / 100) * 10,
            decoration: BoxDecoration(
              color: secondary,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0),
              ),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(-3, -3),
                  spreadRadius: -3,
                  blurRadius: 5,
                  color: primary.withOpacity(.5),
                ),
              ],
            ),
            child: Container(
              padding: const EdgeInsets.all(12.0),
              child: TextButton(
                onPressed: () {},
                child: Container(
                  width: dWidth,
                  padding: const EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    gradient: gradient,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  alignment: Alignment.center,
                  child: const Text(
                    "Konstenlos Registrieren",
                    style: TextStyle(color: secondary, fontSize: 18.0),
                  ),
                ),
              ),
            ),
          )
        : const SizedBox.shrink();
  }
}
