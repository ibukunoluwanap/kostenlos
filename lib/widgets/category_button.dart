import 'package:flutter/material.dart';
import 'package:kostenlos/models/category.dart';
import 'package:kostenlos/utils/colors.dart';

// category filter button widget
Widget categoryButton(
  double dWidth,
  CategoryFilter category,
  int index,
  List categoryList, {
  bool isLarge = false,
  double parentWidth = 0.0,
}) {
  // getting the first and last values of the button list
  int first = categoryList.indexOf(categoryList.first);
  int last = categoryList.indexOf(categoryList.last);

  return Container(
    width: parentWidth != 0.0
        ? parentWidth / 3
        : (dWidth / 100) * 40, // changes the width size based on the viewpoint
    decoration: BoxDecoration(
      color: category.isSelected ? lightGreen : secondary,
      border: Border.all(
        color: primary.withOpacity(0.5),
      ),

      // add borderRadius style to first and last values of the list buttons
      borderRadius: index == first
          ? const BorderRadius.only(
              topLeft: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0),
            )
          : index == last && parentWidth != 0.0
              ? const BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                )
              : BorderRadius.circular(0.0),
    ),
    child: Center(
      child: Text(
        category.title,
        style: TextStyle(
          fontSize: 16.0,
          color: category.isSelected ? secondary : green,
        ),
      ),
    ),
  );
}
