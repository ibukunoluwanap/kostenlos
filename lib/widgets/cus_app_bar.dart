import 'package:flutter/material.dart';
import 'package:kostenlos/utils/colors.dart';
import 'package:kostenlos/utils/responsive.dart';

// custom app bar
class CusAppBar extends StatefulWidget {
  const CusAppBar({Key? key, required this.registerBtnVisibility})
      : super(key: key);
  final bool registerBtnVisibility;

  @override
  State<CusAppBar> createState() => _CusAppBarState();
}

class _CusAppBarState extends State<CusAppBar> {
  @override
  Widget build(BuildContext context) {
    // getting query width and height
    double dWidth = MediaQuery.of(context).size.width;
    double dHeight = MediaQuery.of(context).size.height;

    bool isNotScrolling = widget.registerBtnVisibility;

    // responsive sizes
    bool isDesktop = Responsive.isDesktop(context);

    return Column(
      children: [
        Container(
          width: dWidth,
          height: (dHeight / 100) * 1,
          decoration: BoxDecoration(gradient: gradient),
        ),
        Container(
          width: dWidth,
          height: (dHeight / 100) * 9,
          decoration: BoxDecoration(
            color: secondary,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
            ),
            boxShadow: [
              BoxShadow(
                offset: const Offset(3, 3),
                spreadRadius: -3,
                blurRadius: 5,
                color: primary.withOpacity(.5),
              ),
            ],
          ),
          child: Container(
            margin: const EdgeInsets.only(right: 20.0),
            child: Row(
              children: [
                const Spacer(),
                // show only on mobile viewpoint
                if (isDesktop)
                  if (!isNotScrolling)
                    TextButton(
                      onPressed: () {},
                      child: const Text(
                        "Jetzt Klicken",
                        style: TextStyle(color: primary, fontSize: 18.0),
                      ),
                    ),
                // show only on mobile viewpoint
                if (isDesktop)
                  if (!isNotScrolling)
                    TextButton(
                      onPressed: () {},
                      child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 15.0),
                        padding: const EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 30.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: green),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        alignment: Alignment.center,
                        child: const Text(
                          "Konstenlos Registrieren",
                          style: TextStyle(color: green, fontSize: 18.0),
                        ),
                      ),
                    ),
                // show on all viewpoint
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    "Login",
                    style: TextStyle(color: green, fontSize: 18.0),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
