import 'package:flutter/material.dart';
import 'package:kostenlos/models/category.dart';
import 'package:kostenlos/utils/colors.dart';
import 'package:kostenlos/utils/responsive.dart';
import 'package:kostenlos/widgets/category_button.dart';
import 'package:kostenlos/widgets/cus_app_bar.dart';
import 'package:kostenlos/widgets/cus_bottom_bar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // setting default category data
  int selectedSelector = 0;
  String subtitle = "Drei einfache Schritte zu deinem neuen Job";
  String content1 = "Erstellen dein Lebenslauf";
  String content2 = "Erstellen dein Lebenslauf";
  String content3 = "Mit nur einem Klick bewerben";

  List<CategoryFilter> categoryList = <CategoryFilter>[];

  final ScrollController scrollController = ScrollController();
  bool registerBtnVisibility = true;

  @override
  void initState() {
    // listening to the scrollController to control event changes based on the scroll reponse
    scrollController.addListener(() {
      if (scrollController.position.pixels > 0) {
        registerBtnVisibility = false;
      } else {
        registerBtnVisibility = true;
      }
      setState(() {});
    });
    // setting category list.
    categoryList.add(
      CategoryFilter(
        title: "Arbeitnehmer",
        subtitle: "Drei einfache Schritte zu deinem neuen Job",
        content1: "Erstellen dein Lebenslauf",
        content2: "Erstellen dein Lebenslauf",
        content3: "Mit nur einem Klick bewerben",
        isSelected: true,
      ),
    );
    categoryList.add(
      CategoryFilter(
          title: "Arbeitgeber",
          subtitle: "Drei einfache Schritte zu deinem neuen Mitarbeiter",
          content1: "Erstellen dein Unternehmensprofil",
          content2: "Erstellen ein Jobinserat",
          content3: "Wähle deinen neuen Mitarbeiter aus",
          isSelected: false),
    );
    categoryList.add(
      CategoryFilter(
          title: "Temporärbüro",
          subtitle: "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
          content1: "Erstellen dein Unternehmensprofil",
          content2: "Erhalte Vermittlungs- angebot von Arbeitgeber",
          content3: "Vermittlung nach Provision oder Stundenlohn",
          isSelected: false),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // getting query width and height
    double dWidth = MediaQuery.of(context).size.width;
    double dHeight = MediaQuery.of(context).size.height;

    // responsive sizes
    bool isMobile = Responsive.isMobile(context);
    bool isTablet = Responsive.isTablet(context);

    return Stack(
      children: [
        // clipper can be changed to fit required size and size at utils/clips.dart
        // currently disable

        // ClipPath(
        //   clipper: Clipper(),
        //   child: Container(
        //     color: lightGreen.withOpacity(0.7),
        //   ),
        // ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            controller: scrollController,
            physics: const BouncingScrollPhysics(),
            padding: EdgeInsets.only(
                top: (dHeight / 100) * 12, bottom: (dHeight / 100) * 10),
            child: Column(
              children: [
                _buildHeader(dWidth, isMobile, isTablet),
                _buildCategoryFilter(dWidth, dHeight, isMobile, isTablet),
                _buildSubtitle(dWidth, isMobile, isTablet),
                _buildContain(dWidth, dHeight, isMobile, isTablet),
              ],
            ),
          ),
        ),
        Positioned(
          child: CusAppBar(
            registerBtnVisibility: registerBtnVisibility,
          ),
        ),
        const Positioned(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: CusBottomBar(),
          ),
        ),
      ],
    );
  }

  Widget _buildHeader(dWidth, isMobile, isTablet) {
    return isMobile || isTablet
        ? Column(
            children: [
              const Align(
                alignment: Alignment.topCenter,
                child: Text(
                  "Deine Job \nWebsite",
                  style: TextStyle(
                    color: primary,
                    fontSize: 42.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Image.asset("assets/hero.png")),
            ],
          )
        : SizedBox(
            width: (dWidth / 100) * 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Deine Job \nWebsite",
                        style: TextStyle(
                          color: primary,
                          fontSize: 50.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30.0),
                    registerBtnVisibility
                        ? TextButton(
                            onPressed: () {},
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 30.0),
                              decoration: BoxDecoration(
                                gradient: gradient,
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              alignment: Alignment.center,
                              child: const Text(
                                "Konstenlos Registrieren",
                                style:
                                    TextStyle(color: secondary, fontSize: 18.0),
                              ),
                            ),
                          )
                        : const SizedBox.shrink(),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  child: const CircleAvatar(
                    backgroundColor: secondary,
                    radius: 150.0,
                    backgroundImage: AssetImage("assets/hero.png"),
                  ),
                ),
              ],
            ),
          );
  }

  Widget _buildCategoryFilter(dWidth, dHeight, isMobile, isTablet) {
    return Container(
      width: isMobile || isTablet ? dWidth : (dWidth / 100) * 40,
      height: (dHeight / 100) * 5.0,
      margin: EdgeInsets.only(
          top: isMobile || isTablet ? 10.0 : 100.0, left: 10.0, bottom: 10.0),
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: categoryList.length,
        itemBuilder: (BuildContext context, int index) {
          var categoryItem = categoryList[index];
          return GestureDetector(
            onTap: () {
              setState(
                () {
                  for (var item in categoryList) {
                    item.isSelected = false;
                  }
                  subtitle = categoryItem.subtitle;
                  content1 = categoryItem.content1;
                  content2 = categoryItem.content2;
                  content3 = categoryItem.content3;
                  categoryItem.isSelected = true;
                  selectedSelector = index;
                },
              );
            },
            child: isMobile || isTablet
                ? categoryButton(dWidth, categoryItem, index, categoryList)
                : categoryButton(dWidth, categoryItem, index, categoryList,
                    isLarge: true, parentWidth: (dWidth / 100) * 40),
          );
        },
      ),
    );
  }

  Widget _buildSubtitle(dWidth, isMobile, isTablet) {
    return Container(
      width: isMobile || isTablet ? dWidth : (dWidth / 100) * 30,
      margin: const EdgeInsets.all(20.0),
      alignment: Alignment.topCenter,
      child: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: lightPrimary,
          fontSize: isMobile || isTablet ? 24.0 : 30.0,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  Widget _buildContain(dWidth, dHeight, isMobile, isTablet) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      alignment: Alignment.topCenter,
      child: Stack(
        children: [
          Column(
            children: [
              isMobile || isTablet
                  ? Column(
                      children: [
                        Image.asset(
                          "assets/img1.png",
                          width: (dWidth / 100) * 60,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: [
                            const Text(
                              "1.",
                              style: TextStyle(
                                color: lightPrimary,
                                fontSize: 150.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(width: 20.0),
                            Flexible(
                              child: Text(
                                content1,
                                style: const TextStyle(
                                  color: lightPrimary,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  : SizedBox(
                      width: (dWidth / 100) * 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Flexible(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.ideographic,
                              children: [
                                const Text(
                                  "1.",
                                  style: TextStyle(
                                    color: lightPrimary,
                                    fontSize: 150.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(width: 20.0),
                                Flexible(
                                  child: Text(
                                    content1,
                                    style: const TextStyle(
                                      color: lightPrimary,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Image.asset(
                            "assets/img1.png",
                            width: (dWidth / 100) * 60,
                          ),
                        ],
                      ),
                    ),
              isMobile || isTablet
                  ? Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            textBaseline: TextBaseline.ideographic,
                            children: [
                              const Text(
                                "2.",
                                style: TextStyle(
                                  color: lightPrimary,
                                  fontSize: 150.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(width: 20.0),
                              Flexible(
                                child: Text(
                                  content2,
                                  style: const TextStyle(
                                    color: lightPrimary,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Image.asset(
                            selectedSelector == 1
                                ? "assets/img4.png"
                                : selectedSelector == 2
                                    ? "assets/img6.png"
                                    : "assets/img2.png",
                            width: (dWidth / 100) * 60,
                          )
                        ],
                      ),
                    )
                  : Container(
                      width: (dWidth / 100) * 80,
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Row(
                        children: [
                          Image.asset(
                            selectedSelector == 1
                                ? "assets/img4.png"
                                : selectedSelector == 2
                                    ? "assets/img6.png"
                                    : "assets/img2.png",
                            width: (dWidth / 100) * 60,
                          ),
                          Flexible(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.ideographic,
                              children: [
                                const Text(
                                  "2.",
                                  style: TextStyle(
                                    color: lightPrimary,
                                    fontSize: 150.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(width: 20.0),
                                Flexible(
                                  child: Text(
                                    content2,
                                    style: const TextStyle(
                                      color: lightPrimary,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
              isMobile || isTablet
                  ? Padding(
                      padding: const EdgeInsets.only(left: 80.0),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            textBaseline: TextBaseline.ideographic,
                            children: [
                              const Text(
                                "3.",
                                style: TextStyle(
                                  color: lightPrimary,
                                  fontSize: 150.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(width: 20.0),
                              Flexible(
                                child: Text(
                                  content3,
                                  style: const TextStyle(
                                    color: lightPrimary,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Image.asset(
                            selectedSelector == 1
                                ? "assets/img5.png"
                                : selectedSelector == 2
                                    ? "assets/img7.png"
                                    : "assets/img3.png",
                            width: (dWidth / 100) * 60,
                          )
                        ],
                      ),
                    )
                  : Container(
                      width: (dWidth / 100) * 80,
                      padding: const EdgeInsets.only(left: 80.0),
                      child: Row(
                        children: [
                          Flexible(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.ideographic,
                              children: [
                                const Text(
                                  "3.",
                                  style: TextStyle(
                                    color: lightPrimary,
                                    fontSize: 150.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(width: 20.0),
                                Flexible(
                                  child: Text(
                                    content3,
                                    style: const TextStyle(
                                      color: lightPrimary,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Image.asset(
                            selectedSelector == 1
                                ? "assets/img5.png"
                                : selectedSelector == 2
                                    ? "assets/img7.png"
                                    : "assets/img3.png",
                            width: (dWidth / 100) * 60,
                          )
                        ],
                      ),
                    ),
            ],
          ),
          Positioned(
            top: (dHeight / 100) * 50,
            left: (dWidth / 100) * 10,
            child: Image.asset(
              "assets/arrow1.png",
              width: (dWidth / 100) * 50,
            ),
          ),
          Positioned(
            top: (dHeight / 100) * 140,
            right: (dWidth / 100) * 10,
            child: Image.asset(
              "assets/arrow2.png",
              width: (dWidth / 100) * 60,
              height: (dWidth / 100) * 20,
            ),
          ),
        ],
      ),
    );
  }
}
