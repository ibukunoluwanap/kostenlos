import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostenlos/screens/home.dart';
import 'package:kostenlos/utils/colors.dart';

// setting default landing screen in event of change
Widget _defaultHome = const HomeScreen();

// main application
class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: primary,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: primary),
        textTheme: GoogleFonts.latoTextTheme(Theme.of(context).textTheme),
      ),
      title: "Kostenlos",
      home: _defaultHome,
      initialRoute: "home",
      routes: {
        "home": (_) => const HomeScreen(),
      },
    );
  }
}
