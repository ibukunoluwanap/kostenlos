// category filter model

class CategoryFilter {
  String title;
  String subtitle;
  String content1;
  String content2;
  String content3;
  bool isSelected;

  CategoryFilter(
      {required this.title,
      required this.subtitle,
      required this.content1,
      required this.content2,
      required this.content3,
      this.isSelected = false});
}
