import 'package:flutter/material.dart';

// custom colors
const primary = Color(0xFF2f394a);
const lightPrimary = Color(0xFF718096);
const secondary = Color(0xFFffffff);
const green = Color(0xFF319795);
const lightGreen = Color(0xFF81e6d9);

LinearGradient gradient = const LinearGradient(
  colors: [Color(0xff319698), Color(0xff3183cd)],
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
);
