import 'package:flutter/material.dart';

// responsive class handler
class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget tablet;
  final Widget desktop;

  const Responsive({
    Key? key,
    required this.mobile,
    required this.tablet,
    required this.desktop,
  }) : super(key: key);

  // mobile return
  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 768;

  // tablet return
  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width >= 768 &&
      MediaQuery.of(context).size.width < 1024;

  // desktop return
  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1024;

  @override
  Widget build(BuildContext context) {
    // setting device size
    final Size deviceSize = MediaQuery.of(context).size;

    if (deviceSize.width >= 1024) {
      return desktop; // return desktop if on desktop width viewpoint
    } else if (deviceSize.width >= 768 && deviceSize.width < 1024) {
      return tablet; // return tablet if on tablet width viewpoint
    } else {
      return mobile; // return mobile if on mobile width viewpoint
    }
  }
}
